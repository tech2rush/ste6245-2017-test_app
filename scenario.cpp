#include "scenario.h"
#include "../collision_library_interface/include/collision_interface.h"
#include "../ste6245-2017-072550-collision_library/include/collision_library.h"

#include "testtorus.h"

//// hidmanager
//#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>

// qt
#include <QQuickItem>



void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8, 0.002, 0.0008);
  scene()->insertLight( light, false );

  // Insert Sun
  scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3> init_cam_pos(  0.0f, -10.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  0.0f, 0.0f, 1.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Front cam
  auto front_rcpair = createRCPair("Front");
  front_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, -50.0f, 0.0f ), init_cam_dir, init_cam_up );
  front_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( front_rcpair.camera.get() );
  front_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Side cam
  auto side_rcpair = createRCPair("Side");
  side_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( -50.0f, 0.0f, 0.0f ), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ), init_cam_up );
  side_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( side_rcpair.camera.get() );
  side_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Top cam
  auto top_rcpair = createRCPair("Top");
  top_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, 0.0f, 50.0f ), -init_cam_up, init_cam_dir );
  top_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( top_rcpair.camera.get() );
  top_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );






/*
  // Surface visualizers
  auto surface_visualizer = new GMlib::PSurfNormalsVisualizer<float,3>;

  // Surface
  auto surface = new TestTorus;
  surface->toggleDefaultVisualizer();
  surface->insertVisualizer(surface_visualizer);
  surface->replot(200,200,1,1);
  scene()->insert(surface);

  surface->test01();
  */

  /*
   * Testbed for collisionlibrary, currently handling planes and spheres
   */

  auto controller = new collision::MyController();


  const auto s = 10.0f;
  const std::vector< GMlib::Point<float,3> > vertices =
  {
    GMlib::Point<float,3> (-s, -s, -s),
    GMlib::Point<float,3> ( s, -s, -s),
    GMlib::Point<float,3> ( s,  s, -s),
    GMlib::Point<float,3> (-s,  s, -s),
    GMlib::Point<float,3> (-s, -s,  s),
    GMlib::Point<float,3> ( s, -s,  s),
    GMlib::Point<float,3> ( s,  s,  s),
    GMlib::Point<float,3> (-s,  s,  s)
  };

  const std::vector< int > index =
  {
      0,1,3,1,
      3,2,7,1,
      0,3,4,1,
      1,5,2,1,
      0,4,1,0,
      4,7,5,0
  };

  //Not pretty but works. Move on.
  for (uint i = 0; i < index.size(); i += 4)
  {
      //Crashes with shared_ptr
    auto p = new collision::StaticPPlane
            (vertices.at( index.at(i)),
             vertices.at( index.at(i+1)) - vertices.at( index.at(i)),
             vertices.at( index.at(i+2)) - vertices.at( index.at(i)));
    if (index.at(i+3) == 1) //Visible == TRUE
    {
        p->toggleDefaultVisualizer();
        //auto n_vis = new GMlib::PSurfNormalsVisualizer<float,3>;
        //p->insertVisualizer(n_vis);
        p->replot(1,1,1,1);
    }
    controller->add(p);
    scene()->insert(p);
  }

  auto ball = new collision::DynamicPSphere(1.0f);
  ball->translateParent(GMlib::Vector<float,3>(2.0f, 2.0f, -5.0f));
  ball->velocity = GMlib::Vector<double,3>(-3.7f, -0.0f, 0.0f);
  ball->toggleDefaultVisualizer();
  ball->replot(10,10,1,1);

  auto ball2 = new collision::DynamicPSphere(1.0f);
  ball2->translateParent(GMlib::Vector<float,3>(-2.0f, -2.0f, -5.0f));
  ball2->velocity = GMlib::Vector<double,3>(0.001f, 5.0f, 0.0f);
  ball2->toggleDefaultVisualizer();
  ball2->replot(10,10,1,1);

  auto ball3 = new collision::DynamicPSphere(1.5f);
  ball3->translateParent(GMlib::Vector<float,3>(0.0f, -5.0f, -5.0f));
  ball3->velocity = GMlib::Vector<double,3>(0.000f, 0.0f, 0.0f);
  ball3->toggleDefaultVisualizer();
  ball3->replot(10,10,1,1);

  controller->add(ball);
  controller->add(ball2);
  controller->add(ball3);

  scene()->insert(ball);
  scene()->insert(ball2);
  scene()->insert(ball3);

  scene()->insert(controller);
}

void Scenario::cleanupScenario() {

}
